/**
 * Created by Chanon.S on 11/17/2016.
 */
app.controller("listReceiptController", function ($scope, $sce, $resource, $log, $httpParamSerializer, $route, $routeParams, $filter, receiptCollect,customerAutocomplete) {
    $scope.$parent.menuBar = 'receiptList';
    $scope.receipt_type = $routeParams.type || "product";
    $scope.$parent.receipt_type = $scope.receipt_type;
    $scope.search={};
    $scope.cmp=[];
    $scope.ready = false;
    
    // $scope.textSearch = $routeParams.q || "";
    $scope.init = function () {
        $scope.fetchReceiptCollect();
        
    };

    $scope.table = {
        options: {
            ordering: false,
            searching: false,
            pageLength: 25
        }
    };
    $resource("api/company/related").
    query()
    .$promise
    .then(function(result){
        $scope.cmp=result;
       // console.log($scope.cmp);
    })
    .catch(function(err){

    });
    $scope.onClicksearch =function(){
        $scope.search.startDate =  $scope.search.startDate? new Date($scope.search.startDate).getTime() :null;
        $scope.search.endDate =  $scope.search.endDate? new Date($scope.search.endDate).getTime() :null;
        $resource("api/receipts/:type",{type: $scope.receipt_type},{post: {method: 'post', headers: {'content-type': 'application/json'}}})
            .post(JSON.stringify($scope.search))
            .$promise
            .then(function (result) {
                if (result.state)
                    receiptCollect.collection = $scope.receiptCollect = result.data;
            })
            .catch(function (err) {
            });
            
          //  alert();


    };
    $scope.onClickReset = function(){
        $scope.search = {};
        //console.log($scope.search);
    };
    $scope.fetchReceiptCollect = function () {
        $resource("api/receipts/:type",{type: $scope.receipt_type},{post: {method: 'post', headers: {'content-type': 'application/json'}}})
            .post($scope.search)
            .$promise
            .then(function (result) {
                if (result.state)
                    receiptCollect.collection = $scope.receiptCollect = result.data;
                    $scope.initAutoComplete();
            })
            .catch(function (err) {
            });
    };

    $scope.del = function (receipt) {
        alertify.confirm().set({
            message: "Delete receipt no. " + receipt.receiptID + " ?<br><br><label>Reason :</label><input class='form-control' id='reason_del' placeholder='[Required]'>",
            onok: function () {
                $scope.isWaiting = true;
                var input = $("#reason_del");
                if (!input.val()) {
                    input.focus();
                    return false;
                }

                $resource('api/receipt/:id')
                    .delete({id: receipt.id, reason: input.val()})
                    .$promise
                    .then(function (result) {
                        if (result.state == true) {
                            receipt.reason = input.val();
                            receipt.cancel = 1;
                        }
                    })
                    .catch(function (err) {
                    });
            }
        }).setHeader("Delete").show();
    };

    $scope.initAutoComplete = function () {
        var apiAutoComplete = customerAutocomplete.collection['ALL'] || [];
        if (apiAutoComplete.length < 1) {
            $resource('api/customersearch/:cmp')
                .query({cmp:'all'})
                .$promise
                .then(function (result) {
                    customerAutocomplete.collection['ALL'] = result;
                    $scope.setupAutocomplete(customerAutocomplete.collection['ALL']);
                })
                .catch(function (err) {
                    throw err;
                });
        } else {
            $scope.setupAutocomplete(customerAutocomplete.collection['ALL']);
        }
    };
    $scope.onSelected = function(suggestion){
       
             $scope.search.customer = suggestion.data;
           //console.log( suggestion.value);
         

    };
    $scope.setupAutocomplete = function (collection) {
        $("#customer_search").autocomplete({
            lookup: collection,
            lookupLimit: 150,
            orientation: "auto",
            onSelect: $scope.onSelected
            
        });

        $scope.ready = true;
    };

    $scope.init();
});