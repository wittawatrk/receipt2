
var app = angular
    .module("receiptApp", ["ngRoute", "ngMaterial", "ngMessages", "ngResource",'datatables'])
    .config(function ($routeProvider, $httpProvider, $locationProvider, $mdDateLocaleProvider) {
        $locationProvider.html5Mode(true);
        $routeProvider
            .when("/dist/receipt_list/:type", {
                templateUrl: "public/html/receipt_list.template.html",
                controller: "listReceiptController"
            })
            .when("/dist/add_receipt/:type", {
                templateUrl: "public/html/receipt_add.template.html",
                controller: "addReceiptController"
            })
            .when("/dist/set_customer/:id", {
                templateUrl: 'public/html/receipt_set_customer.template.html',
                /*templateUrl: function (params) {
                    return "./set_customer.php?id=" + params.id;
                },*/
                controller: "setCustomerController"
            })
            .when("/dist/edit_receipt/:id", {
                templateUrl: 'public/html/receipt_editor.template.html',
                /*templateUrl: function (params) {
                 return "./edit_receipt.php?id=" + params.id;
                 },*/
                controller: "editReceiptController"
            })
            .otherwise({
                redirectTo: "/dist/receipt_list/product"
            });


        $httpProvider.defaults.headers.post["X-Requested-With"] = "XMLHttpRequest";
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

        $mdDateLocaleProvider.formatDate = function (date) {
            if (!date)return null;
            var
                d = (('00' + date.getDate()).slice(-2)),
                m = (('00' + (1 + date.getMonth())).slice(-2)),
                y = date.getFullYear();
            return d + "/" + m + "/" + y;
        };
    })
    .service("receiptCollect", function () {
        this.collection = [];
    })
    .service("customerAutocomplete", function () {
        this.collection = {};
    })
    .service("invoiceAutocomplete", function () {
        this.collection = {};
    })
    /*.filter("myReceiptCollect", function () {
        var arrField = ["receiptID", "customerName", "receiptDate", "inv_collect"];
        return function (objCollect, keyword) {
            if (keyword != undefined && keyword != "") {
                var newObj = [];
                for (var index = 0; index < objCollect.length; index++) {
                    var arr = [];
                    for (var i = 0; i < arrField.length; i++) {
                        arr.push(objCollect[index][arrField[i]] || "");
                        // str_index_key += object[arrField[i]] + " ";
                    }
                    var str_index_key = JSON.stringify(arr);
                    str_index_key = str_index_key.replace(/\,\"\$\$hashKey\"\:\"object\:[0-9]{1,}\"/gi, "");
                    str_index_key = str_index_key.replace(/(\"[_a-zA-Z]{1,}\"\:|\}|\{|\]|\[|\")/gi, "");
                    str_index_key = str_index_key.replace(/,/gi, " ");
                    str_index_key = str_index_key.replace(/\s+/gi, " ");
                    var newKeyword = "(?=.*" + keyword.replace(/\s+/gi, ")(?=.*") + ")";
                    if (str_index_key.match(new RegExp(newKeyword, "gi"))) {
                        newObj.push(objCollect[index]);
                    }
                }
                return newObj;
            }
            return objCollect
        };
    });
*/
Array.prototype.summary = function () {
    var price = 0, vat = 0, price_vat = 0, pay = 0;
    for (var i = 0; i < this.length; i++) {
        price += eval((this[i].amount + "").replace(/,/gi, ""));
        vat += eval((this[i].vat + "").replace(/,/gi, ""));
        price_vat += eval((this[i].total + "").replace(/,/gi, ""));
        pay += eval((this[i].pay_amount + "").replace(/,/gi, ""))
    }
    return {price: price, vat: vat, price_vat: price_vat, pay: pay}
};