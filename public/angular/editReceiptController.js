/**
 * Created by Chanon.S on 11/22/2016.
 */

app.controller("editReceiptController", function ($scope, $log, $resource, $route, $timeout, $sce, $http, $filter, $routeParams, $httpParamSerializer, invoiceAutocomplete) {
    $scope.settleResource = $resource('api/settle/:id');
    $scope.$parent.menuBar = 'receiptEditor';
    $scope.uniqueId = $routeParams.id;
    $scope.saveLabel = $sce.trustAsHtml("<span class='glyphicon glyphicon-floppy-save'></span> SAVE");
    $scope.dropdownAccount = [];
    $scope.invoiceCollect = [];
    $scope.formdata = [];
    $scope.isSaveDisabled = false;
    $scope.ready = false;

    $scope.bindData = function () {
        $scope.limitInv = $scope.masterReceipt.receiptType == "service" ? 1 : 10;
        $scope.blankInv = $scope.count($scope.limitInv);
        $scope.getInvoiceCollect(function () {
            $scope.blankInv = $scope.count($scope.limitInv - $scope.invoiceCollect.length);
            $scope.getInvAutComplete();
            $scope.summary();
            $scope.ready = true;
        });
        $scope.getAccountAutoComplete();
        $scope.getBankAutoComplete();
        $scope.getFormType();

        $scope.masterReceipt.settles = $.map($scope.masterReceipt.settles, function (value, index) {
            return value;
        });
        
    };
   
    
    $scope.getAccountAutoComplete = function () {
        $resource('api/account/autoComplete/:cmp')
            .query({cmp: $scope.masterReceipt.organization})
            .$promise
            .then(function (accounts) {
                $scope.dropdownAccount = accounts;
            })
            .catch(function (err) {
                throw err;
            })
    };

    $scope.getBankAutoComplete = function () {

        $resource('api/bank/autoComplete')
            .query()
            .$promise
            .then(function (banks) {
                $scope.dropdownBank = banks;
            })
            .catch(function (err) {
                throw err;
            })
    };
    $scope.getFormType=function (){

        $resource('api/form/:type')
        .query({type:$scope.masterReceipt.receiptType})
        .$promise
        .then(function(result){
        $scope.formdata = result;
           })
    .catch(function(err){

});
}    
    $scope.onClickSave = function () {
       
        if ($scope.isSaveDisabled)return false;
        if (!$scope.masterReceipt.receiptDate) {
            $("#receipt_date").focus();
            return false;
        }
        $scope.isWaiting = true;
        
        $scope.saveLabel = $sce.trustAsHtml("Processing...");
        
        var masterReceipt = {};
		angular.copy($scope.masterReceipt, masterReceipt);
        var settleCollect = [];
          masterReceipt.settles.forEach(function (element, key) {
            if (element.payDate)
                element.payDateTime = element.payDate.getTime();
            else
                element.payDateTime = "";

        });
        
        
		masterReceipt.receiptDate = !masterReceipt.receiptDate ? null : masterReceipt.receiptDate.getTime();
        masterReceipt.invoiceCollect = $scope.invoiceCollect;
        $resource('api/receipt/:id', {id: $scope.masterReceipt.id}, {patch: {method: 'patch', headers: {'content-type': 'application/json'}}})
            .patch(masterReceipt)
            .$promise
            .then(function (result) {
                if (result.state === true)
               
                    $route.reload();
                else {
                    $scope.isWating = false;
                    $scope.saveLabel = $sce.trustAsHtml("<span class='glyphicon glyphicon-floppy-save'></span> SAVE");
                }
            })
            .catch(function (err) {
                throw err;
            });
    };

    $scope.addComma = function (num) {
        num = num.replace(/,/gi, "");
        return $filter("number")(num, 2);
    };

    $scope.removeText = function (text) {
        return (text.replace(/[^0-9\.,]/gi, "") || "").trim();
    };

    $scope.toJsDate = function (date) {
        return date ? new Date(date) : date;
    };

    $scope.count = function (size) {
        var arr = [];
        for (var i = 0; i < size; i++) {
            arr.push(i);
        }
        return arr;
    };

    $scope.getInvAutComplete = function () {
        $resource('api/invoice/autoComplete/:cmp/:customerId')
            .query({cmp: $scope.masterReceipt.organization, customerId: $scope.masterReceipt.customerId})
            .$promise
            .then(function (result) {
                $scope.masterInvoiceAutoComplete = result;

                for (var i = 0; i < $scope.invoiceCollect.length; i++) {
                    $scope.masterInvoiceAutoComplete = $scope.masterInvoiceAutoComplete.filter(function (a) {
                        return ($scope.invoiceCollect[i].id || "").trim() != (a.data.ID || "").trim();
                    })
                }

                $scope.setInvoiceAutoComplete($scope.masterInvoiceAutoComplete);
            })
            .catch(function (err) {
                throw err;
            })
    };

    $scope.onInvInvalidateSelection = function () {
        $scope.lastSuggestion = null;
        $scope.tempInv = {};
        $scope.$apply()
    };

    $scope.onInvSelected = function (suggestions) {
        if ($scope.lastSuggestion == suggestions)return false;
        $scope.isWaiting = true;
        $scope.lastSuggestion = suggestions;
        $scope.tempInv = {contract_id: "fetching..."};
        $scope.$apply();
        $resource('api/invoice/:cmp/:id')
            .get({cmp: $scope.masterReceipt.organization, id: suggestions.data.ID})
            .$promise
            .then(function (invDetail) {
                $scope.tempInv = invDetail;
                $scope.isWaiting = false;
                $timeout(function () {
                    $("#btn_add").focus();
                }, 100);
            })
            .catch(function (err) {
                throw err;
            })
    };

    $scope.setInvoiceAutoComplete = function (collection) {
        $("#inv_search").autocomplete({
            lookup: collection,
            lookupLimit: 150,
            orientation: "auto",
            onInvalidateSelection: $scope.onInvInvalidateSelection,
            onSelect: $scope.onInvSelected
        })
    };

    $scope.addInvoice = function () {
        if ($scope.invoiceCollect.length >= $scope.limitInv)return false;

        $scope.isWaiting = true;

        $scope.checkPaid(function (err, paid) {
            if (!err) {
                $scope.tempInv.paid = paid;
                $scope.tempInv.pay_amount = $filter("number")(+$scope.tempInv.total - +paid, 2);
                $scope.invoiceCollect.push($scope.tempInv);
                $scope.masterInvoiceAutoComplete = $scope.masterInvoiceAutoComplete.filter(function (object) {
                    return object.data.ID != $scope.tempInv.id;
                });
                $scope.setInvoiceAutoComplete($scope.masterInvoiceAutoComplete);
                $scope.tempInv = {};
                $scope.inv_search = null;
                $scope.blankInv = $scope.count($scope.limitInv - $scope.invoiceCollect.length);
                $scope.isWaiting = false;
                $scope.summary();
                setTimeout(function () {
                    $("#inv_search").focus();
                }, 200);
            } else {
                console.error(err);
                $scope.isWaiting = false;
            }
        });
    };

    $scope.removeInvoice = function (index, data) {
        $scope.masterInvoiceAutoComplete.push({
            value: data.inv_id,
            data: {
                ID: data.id,
                InvID: data.inv_id
            }
        });
        $scope.masterInvoiceAutoComplete = $scope.masterInvoiceAutoComplete.sort(function (a, b) {
            return +a.value - +b.value;
        });
        $scope.invoiceCollect.splice(index, 1);
        $scope.blankInv = $scope.count($scope.limitInv - $scope.invoiceCollect.length);
        $scope.summary();
    };

    $scope.checkPaid = function (callback) {
        $resource('api/invoice/paid/:cmp/:invId/:rUniqueId')
            .get({
                invId: $scope.tempInv.inv_id,
                cmp: $scope.masterReceipt.organization,
                rUniqueId: $scope.masterReceipt.id
            })
            .$promise
            .then(function (result) {
                callback(null, result.paid);
            })
            .catch(function (err) {
                callback(err);
            })
    };

    $scope.getInvoiceCollect = function (callback) {
        $resource('api/invoices/:id')
            .query({id: $scope.masterReceipt.id})
            .$promise
            .then(function (invoices) {
                $scope.invoiceCollect = invoices;
                callback();
            })
            .catch(function (err) {
                throw err;
            });
    };

    $scope.summary = function () {
        $scope.sum = $scope.invoiceCollect.summary();
    };

    $scope.checked = function () {
    };

    $scope.checkOver = function (pay_amount, total_amount, paid_amount) {
        var total = (+total_amount);
        var paid = (+paid_amount);
        var pay = (+(pay_amount.replace(/,/gi, '')));
        return $scope.isSaveDisabled = pay > ((+$scope.payBalance(total, paid).toFixed(2)));
    };

    $scope.payBalance = function (total, paid) {
        return ((+total) - (Math.abs(+paid)));
    };

    $resource('api/receipt/:id')
        .get({id: $scope.uniqueId})
        .$promise
        .then(function (receiptDetail) {
            $scope.masterReceipt = receiptDetail;
			$log.info($scope.masterReceipt.receiptDate);
            $scope.masterReceipt.receiptDate = $scope.toJsDate($scope.masterReceipt.receiptDate);
            $scope.masterReceipt.settles = [];
            $scope.formtype = $scope.masterReceipt.receiptType;
            if($scope.masterReceipt.receiptDate){
                $scope.minDate = new Date(
                $scope.masterReceipt.receiptDate.getFullYear(),
                $scope.masterReceipt.receiptDate.getMonth(),
                1
               // $scope.masterReceipt.receiptDate.getDate()
            );
        
            $scope.maxDate = new Date(
                $scope.masterReceipt.receiptDate.getFullYear(),
                $scope.masterReceipt.receiptDate.getMonth()+1,
                0
                // $scope.masterReceipt.receiptDate.getDate()
            );}
            if(!$scope.masterReceipt.receiptCase)
                $scope.masterReceipt.receiptCase = 'normal';
            return $resource('api/customer/:cmp/:id').get({cmp: $scope.masterReceipt.organization, id: $scope.masterReceipt.rawCustomerId}).$promise;
        })
        .then(function (customerDetail) {
            $scope.masterReceipt.taxId = (customerDetail || {}).CustomerTaxID;
            return $scope.settleResource.get({id: $scope.masterReceipt.settleId[0]}).$promise;
        })
        .then(function (settleDetail) {
            $scope.masterReceipt.settles[0] = settleDetail;
            return $scope.settleResource.get({id: $scope.masterReceipt.settleId[1]}).$promise;
        })
        .then(function (settleDetail) {
            $scope.masterReceipt.settles[1] = settleDetail;
            return $scope.settleResource.get({id: $scope.masterReceipt.settleId[2]}).$promise;
        })
        .then(function (settleDetail) {
            $scope.masterReceipt.settles[2] = settleDetail;
            $scope.bindData();
        })
        .catch(function (err) {
            throw err;
        });


    
        
    
});
