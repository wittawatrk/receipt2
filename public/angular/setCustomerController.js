/**
 * Created by Chanon.S on 11/16/2016.
 */
app.controller("setCustomerController", function ($scope, $routeParams, $resource, $location, $sce, receiptCollect, customerAutocomplete) {
    $scope.$parent.menuBar = 'receiptSetCustomer';
    $scope.receipt_id = $routeParams.id;
    $scope.isWaiting = true;
    $scope.ready = false;
    $scope.nextLabel = $sce.trustAsHtml('NEXT <span class="glyphicon glyphicon-arrow-right"></span>');
    $scope.receipt_date = new Date();
    $scope.search_q = null;
    $scope.tax_extra = null;
    $scope.masterReceipt = {};

    $scope.onSubmitted = function () {
        receiptCollect.clear();
    };

    $scope.onClickNext = function () {
        if (!$scope.customer_search) {
            $scope.customer_id = null;
            $("#customer_search").focus();
            return false;
        }
        if (!$scope.customer_id) {
            $scope.customer_search = null;
            $("#customer_search").focus();
            return false;
        }
              if (!$scope.receipt_date) {
             $("#receipt_date").focus();
             return false;
         }
        if (!$scope.customer_name) {
            $("#customer_name").focus();
            return false;
        }
        if (!$scope.customer_address) {
            $("#customer_address").focus();
            return false;
        }
        /*if (!$scope.tax_id)
         return false;*/
         var d = new Date($scope.receipt_date);
        $scope.isWaiting = true;
        $scope.nextLabel = $sce.trustAsHtml('Processing...');
        $resource('api/receipt/customer/:id', {id: $scope.masterReceipt.id})
            .save({
                receipt_id: $scope.masterReceipt.receiptId,
                receipt_date: !!$scope.receipt_date ? d.getTime() : null ,
                raw_customer_id: $scope.raw_customer_id,
                customer_id: $scope.customer_id,
                customer_name: $scope.customer_name,
                customer_address: $scope.customer_address,
                tax_extra: !!$scope.tax_id ? $scope.tax_extra : null
                
            })
            .$promise
            .then(function (result) {
                if (result.state) {
                    $location.path('dist/edit_receipt/' + result.id);
                   
                    
                } else {
                    $scope.isWaiting = false;
                }
            })
            .catch(function (err) {
                $scope.isWaiting = false;
                throw err;
            });
    };

    $scope.initAutoComplete = function () {
        var apiAutoComplete = customerAutocomplete.collection[$scope.masterReceipt.organization] || [];
        if (apiAutoComplete.length < 1) {
            $resource('api/customers/:cmp')
                .query({cmp: $scope.masterReceipt.organization})
                .$promise
                .then(function (result) {
                    customerAutocomplete.collection[$scope.masterReceipt.organization] = result;
                    $scope.setupAutocomplete(customerAutocomplete.collection[$scope.masterReceipt.organization]);
                })
                .catch(function (err) {
                    throw err;
                });
        } else {
            $scope.setupAutocomplete(customerAutocomplete.collection[$scope.masterReceipt.organization]);
        }
    };

    $scope.onCustomerSearch = function () {
        if ($scope.customer_search != $scope.search_q) {
            $scope.tax_id = null;
            $scope.customer_id = null;
            $scope.raw_customer_id = null;
        }
    };

    $scope.onCustomerSelected = function (suggestion) {
        if (suggestion.value != $scope.customer_search) {
            $resource('api/customer/:cmp/:customerId')
                .get({cmp: $scope.masterReceipt.organization, customerId: suggestion.data})
                .$promise
                .then(function (result) {
                    $scope.raw_customer_id = result.RawCustomerID;
                    $scope.customer_address = result.CustomerAddress;
                    $scope.customer_id = result.CustomerID;
                    $scope.customer_name = result.CustomerName;
                    $scope.tax_id = result.CustomerTaxID;
                    $scope.tax_extra = null;
                })
                .catch(function (err) {
                    throw err;
                });
        }
        $scope.customer_search = $scope.search_q = suggestion.value;
    };

    $scope.setupAutocomplete = function (collection) {
        $("#customer_search").autocomplete({
            lookup: collection,
            lookupLimit: 150,
            orientation: "auto",
            onSearchStart: $scope.onCustomerSearch,
            onSelect: $scope.onCustomerSelected
        });

        $scope.ready = true;
    };

    $resource('api/receipt/:id')
        .get({id: $scope.receipt_id})
        .$promise
        .then(function (result) {
            $scope.masterReceipt = result;
            $scope.isWaiting = false;
            $scope.initAutoComplete();
        })
        .catch(function (err) {
        })
    
    
});