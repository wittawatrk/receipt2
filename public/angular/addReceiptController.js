/**
 * Created by Chanon.S on 11/11/2016.
 */

app.controller("addReceiptController", function ($scope, $sce, $resource, $routeParams, $location) {
    $scope.$parent.menuBar = 'addReceipt/' + $routeParams.type;
    $scope.nextLabel = $sce.trustAsHtml("NEXT <span class='glyphicon glyphicon-arrow-right'></span>");
    $scope.isWaiting = true;
    $scope.ready = false;
    $scope.autoReceiptID = null;
    $scope.custom_receipt_id = null;
    $scope.problemMsg = null;
    $scope.receipt_type = $routeParams.type;
    $scope.placeholder = $scope.receipt_type == "product" ? "RC-___-__-__-___" : $scope.receipt_type == "service" ? "___-S_______" :$scope.receipt_type == "service_novat"?"___-_______": "";
    $scope.pattern = $scope.receipt_type == "product" ? "^(0[1-9]|1[0-2])-[0-9]{3,3}$" : $scope.receipt_type == "service" ? "^(0[1-9]|1[0-2])[0-9]{3,3}$" : $scope.receipt_type == "service_novat" ? "^(0[1-9]|1[0-2])[0-9]{3,3}$" : "^$";
    $scope.maxlength = $scope.receipt_type == "product" ? 6 : $scope.receipt_type == "service" ? 5 :$scope.receipt_type == "service_novat" ? 5: 0;

    $resource('api/company/related')
        .query()
        .$promise
        .then(function (result) {
            $scope.relatedCompany = result;
            $scope.isWaiting = false;
            $scope.ready = true;
        })
        .catch(function (err) {
            $scope.ready = true;
            $scope.getCommonProblemMsg();
        });

    $scope.changedCompany = function () {
        $scope.receipt_id = "Calculating...";
        $scope.isWaiting = true;
        $resource('api/receipt/:type/:cmp/availableId')
            .get({type: $scope.receipt_type, cmp: ($scope.company_sn)})
            .$promise
            .then(function (result) {
                $scope.receipt_id = $scope.autoReceiptID = result.receiptId;
                $scope.isWaiting = false;
            })
            .catch(function (err) {
                $scope.getCommonProblemMsg();
            });
    };

    $scope.onClickNext = function () {
        $scope.isWaiting = true;
        $scope.nextLabel = $sce.trustAsHtml("Processing...");
        $resource('api/receipt/:type/:cmp/:id', {type: $scope.receipt_type, cmp: $scope.company_sn, id: $scope.receipt_id})
            .save()
            .$promise
            .then(function (result) {
                if (result.state)
                    $location.path('dist/set_customer/' + result.uniqueId);
                else
                    $scope.getCommonProblemMsg();
            })
            .catch(function (err) {
                $scope.getCommonProblemMsg();
            });
    };

    $scope.onCustomID = function () {
        $scope.isWaiting = true;
        if ($scope.formAddReceipt.custom_receipt_id.$valid) {
            if ($scope.custom_receipt_id) {
                $scope.receipt_id = "Calculating...";
                $resource('api/receipt/:type/:cmp/verifyReceiptId/:id')
                    .get({type: $scope.receipt_type, cmp: $scope.company_sn, id: $scope.custom_receipt_id})
                    .$promise
                    .then(function (result) {
                        $scope.alreadyExist = result.alreadyExist;
                        if (!$scope.alreadyExist) {
                            $scope.receipt_id = result.receiptId;
                            $scope.isWaiting = false;
                        }
                    })
                    .catch(function (err) {
                        $scope.getCommonProblemMsg();
                    });
            } else {
                $scope.alreadyExist = false;
                $scope.receipt_id = $scope.autoReceiptID;
                $scope.isWaiting = false;
            }
        }
    };

    $scope.getCommonProblemMsg = function () {
        $scope.problemMsg = $sce.trustAsHtml("ดูเหมือนฉันพบข้อผิดพลาด&nbsp;&nbsp;กด <a onclick='window.location.reload(1)' style='cursor: pointer'>Refresh</a> ฉัน แล้วลองใหม่อีกครั้ง");
    }
});