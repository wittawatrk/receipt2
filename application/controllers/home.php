<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller
{
    public $uiLogout;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('sso', array('clientId' => CLIENT_ID));
        $ssoInfo = $this->sso->getAuthentication();
        $this->session->userInfo = $ssoInfo['personDetail'];
        $this->uiLogout = $ssoInfo['panelLogout'];
    }

    public function index()
    {

        $this->load->view('home', array('uiLogout' => $this->uiLogout));
    }
}