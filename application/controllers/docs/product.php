<?php

include(APPPATH.'libraries/TCPDF/TCPDF.php');

class product extends CI_Controller
{
    public $receiptDb = null;
    public $userInfo = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('PDF');
        $this->receiptDb = $this->load->database('receipt', true);
        $this->userInfo = $this->session->userInfo;
    }

    public function index($uniqueReceiptId)
    {
        $arr_month = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");

        $queryReceiptDetail = $this->receiptDb->query("SELECT TOP 1 * FROM ReceiptCollect WHERE ID=? AND Cancel=0 AND Show=1 AND Revision>0 AND ReceiptType='product'", array($uniqueReceiptId));
        if (!$queryReceiptDetail || $queryReceiptDetail->num_rows() < 1) {
            HttpResp::code(401);
            die();
        }

        $receiptDetail = $queryReceiptDetail->row_array();

        $queryCustomerDetail = $this->receiptDb->query("findCustomerDetail @ID=?, @dtsource=?", array($receiptDetail['RawCustomerID'], $receiptDetail['Organization']));
        $customerDetail = $queryCustomerDetail->row_array();
        if($receiptDetail["showdate"]==0){
            $date = date_parse($receiptDetail["ReceiptDate"]);
            $year = $date["year"];
            $str_month = $arr_month[$date["month"]];
            $day = substr("00" . $date["day"], -2);
            }
            else{
                $year = '';
                $str_month = '';
                $day = '';
            }

        $settle_collect = array();
        $arrSettleField = array("SettleID1", "SettleID2", "SettleID3");
        foreach($arrSettleField as $settleField) {
            $querySettleDetail = $this->receiptDb->query("SELECT TOP 1 PayType, PayAmount, convert(VARCHAR(20),PayDate,103) AS PayDate, ChequeNo, BankID, AccountID FROM SettleCollect WHERE ID=?"
                , array($receiptDetail[$settleField]));

            $settleDetail = $querySettleDetail->row_array();

            switch($settleDetail['PayType']){
                case 'transfer':
                    if(isset($settleDetail['AccountID']) && !empty($settleDetail['AccountID'])){
                        $queryAccount = $this->receiptDb->query("SELECT tb1.AccountNo, tb2.NameT FROM AccountCollect tb1 LEFT JOIN BankCollect tb2 ON tb1.BankID = tb2.ID WHERE tb1.ID=?", array($settleDetail['AccountID']));
                        $resultAccount = $queryAccount->row_array();
                        $settleDetail["AccountNo"] = $resultAccount["AccountNo"];
                        $settleDetail["BankName"] = $resultAccount["NameT"];
                    }
                    break;
                case 'cheque':
                    if (isset($settleDetail["BankID"]) && !empty($settleDetail["BankID"])) {
                        $queryBank = $this->receiptDb->query("select NameT from BankCollect where ID=?", array($settleDetail['BankID']));
                        $resultBank = $queryBank->row_array();
                        $settleDetail["BankName"] = $resultBank["NameT"];
                    }
                    break;
            }
            array_push($settle_collect, $settleDetail);
        }

        $execFindInvoices = $this->receiptDb->query("findInvoiceCollect @id=?", array($uniqueReceiptId));
        if($execFindInvoices)
            $invoice_collect = $execFindInvoices->result_array();

        Utility::assign($receipt_id, Utility::IdBuilder($receiptDetail));
        Utility::assign($str_date, $day . "  " . $str_month . "  " . $year);
        Utility::assign($term, $receiptDetail["Term"]);
        Utility::assign($tax_id, $customerDetail["tax_id"]);
        Utility::assign($tax_extra, !empty($tax_id) ? $receiptDetail["TaxExtra"] : "");
        Utility::assign($customer_name, $receiptDetail["CustomerName"]);
        Utility::assign($customer_address, $receiptDetail["CustomerAddress"]);

        $totalPay = 0;

        $pdf = $this->pdf;
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $breakMargin = $pdf->getBreakMargin();
        $autoPageBreak = $pdf->getAutoPageBreak();

        // write customer name
        $pdf->SetXY($pdf->GetX() + 15, $pdf->GetY() -1);
        $pdf->MultiCell(125, $pdf->CELL_H * 2, $customer_name, 0, "L", false, 0);

        // write receipt id
        $pdf->SetX($pdf->GetX() + 13);
        $pdf->Cell(40, $pdf->CELL_H, $receipt_id, 0, 1, "L");

        // write tax id
        $pdf->SetXY($pdf->GetX() + 35, $pdf->GetY() + 2);
        $pdf->Cell(105, $pdf->CELL_H, $tax_id . " " . $tax_extra, 0, 0, "L");

        // write tax id
        $pdf->SetX($pdf->GetX() + 13);
        $pdf->Cell(40, $pdf->CELL_H, $str_date, 0, 1, "L");

        // write customer address
        $pdf->SetXY($pdf->GetX() + 15, $pdf->GetY() + 0.1);
        $pdf->MultiCell(125, $pdf->CELL_H * 2, $customer_address, 0, "L", false, 0);

        // write term
        $pdf->SetXY($pdf->GetX() + 13, $pdf->GetY() + 2);
        $pdf->Cell(40, $pdf->CELL_H, $term, 0, 1, "L");

        // write invoice list
        $file = fopen(dirname(__FILE__) . "/product/normalForm.html", "r");
        $html = fread($file, filesize(dirname(__FILE__) . "/product/normalForm.html"));
        $html = mb_eregi_replace("\>\s+\<", "><", $html);
        for ($i = 0; $i < 10; $i++) {
            if (isset($invoice_collect[$i])) {
                $invoice = $invoice_collect[$i];
                $html = mb_ereg_replace("{{no_" . $i . "}}", ($i + 1), $html);
                $html = mb_ereg_replace("{{inv_" . $i . "}}", "เลขที่ INV # " . $invoice["inv_id"], $html);
                $html = mb_ereg_replace("{{date_" . $i . "}}", $invoice["inv_date"], $html);
                $html = mb_ereg_replace("{{ctr_" . $i . "}}", $invoice["contract_id"], $html);
                $html = mb_ereg_replace("{{amount_" . $i . "}}", number_format($invoice["pay_amount"], 2), $html);
                $totalPay += (+$invoice["pay_amount"]);
            } else {
                $html = mb_ereg_replace("{{(no|inv|date|ctr|amount)_" . $i . "}}", "", $html);
            }
        }
        
        $pdf->SetXY($pdf->getLMargin(), 56);
        $pdf->writeHTML($html);

        // write text total
        if($receiptDetail["Remark"]!=''){
        $pdf->SetXY($pdf->getLMargin()+2 , 105);
        $pdf->Cell(115, $pdf->CELL_H, "Remark: ".$receiptDetail["Remark"], 0, 0, "C");
        }
        $pdf->SetXY($pdf->getLMargin() + 32, 115);
        $pdf->Cell(115, $pdf->CELL_H, Utility::toThaiMoney($totalPay), 0, 0, "C");

        // write number total
        $pdf->SetX($pdf->GetX() + 18);
        $pdf->Cell(27, $pdf->CELL_H, number_format($totalPay, 2, ".", ","), 0, 1, "R");

        // var_dump($settle_collect);
        $posY = array(124, 132, 140);
        $i = 0;
        foreach ($settle_collect as $settle) {
            $field_1 = $field_2 = $field_3 = "";

            if ($settle["PayType"] == "transfer")
                $field_1 = $settle["AccountNo"];
            else if ($settle["PayType"] == "cheque")
                $field_1 = $settle["ChequeNo"];

            if ($settle["PayType"] == "transfer")
                $field_2 = $settle["BankName"] . ((+$settle["PayAmount"] > 0) ? (" = " . number_format($settle["PayAmount"], 2, ".", ",")) : "");
            else if ($settle["PayType"] == "cheque")
                $field_2 = $settle["BankName"] . ((+$settle["PayAmount"] > 0) ? (" = " . number_format($settle["PayAmount"], 2, ".", ",")) : "");
            else if ($settle["PayType"] == "cash")
                $field_2 = "เงินสด" . ((+$settle["PayAmount"] > 0) ? (" = " . number_format($settle["PayAmount"], 2, ".", ",")) : "");

            $field_3 = $settle["PayDate"];

            $pdf->setCellHeightRatio(0.8);

            // write field 1
            $pdf->SetXY($pdf->getLMargin() + 60, $posY[$i]);
            $pdf->MultiCell(35, $pdf->CELL_H * 1.2, $field_1, 0, "L", false, 0);

            // write field 2
            $pdf->SetX($pdf->GetX() + 10);
            $pdf->SetFontSize(12);
            $pdf->MultiCell(53, $pdf->CELL_H * 1.4, $field_2, 0, "L", false, 0);
            $pdf->SetFontSize($pdf->FONT_SIZE);

            // write field 3
            $pdf->SetX($pdf->GetX() + 6);
            $pdf->MultiCell(30, $pdf->CELL_H * 1.2, $field_3, 0, "C", false, 0);

            $pdf->Ln();
            $i++;
        }
        $pdf->Output("ProductReceipt.pdf");
    }
}