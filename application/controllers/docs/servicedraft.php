<?php
include(APPPATH . 'libraries/TCPDF/TCPDF.php');
include(APPPATH . 'libraries/PDF/PDF.php');
include(APPPATH . 'libraries/PDF/service/draftFormPDF.php');
include(APPPATH . 'libraries/MapVal.php');

class servicedraft extends CI_Controller
{
    public $receiptDb = null;
    public $userInfo = null;

    public function __construct()
    {
        parent::__construct();
        $this->receiptDb = $this->load->database('receipt', true);
        $this->userInfo = $this->session->userInfo;
    }

    public function index($uniqueReceiptId, $docType)
    {
       
        $docType = mb_strtolower($docType);
        $arr_month = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $id = trim($uniqueReceiptId);

        $queryReceiptDetail = $this->receiptDb->query("SELECT TOP 1 * FROM ReceiptCollect WHERE ID=? AND Cancel=0 AND Show=1 AND Revision>0 AND ReceiptType='service'", array($id));
        if (!$queryReceiptDetail || $queryReceiptDetail->num_rows() < 1) {
            HttpResp::code(401);
            die();
        }

        $receiptDetail = $queryReceiptDetail->row_array();
        $docCase = mb_strtolower($receiptDetail['ReceiptCase']);

        $queryCompanyDetail = $this->receiptDb->query("findCompanyDetail @ShortName=?", array($receiptDetail['Organization']));
        $companyDetail = $queryCompanyDetail->row_array();

        $queryCustomerDetail = $this->receiptDb->query("findCustomerDetail @ID=?, @dtsource=?", array($receiptDetail['RawCustomerID'], $receiptDetail['Organization']));
        $customerDetail = $queryCustomerDetail->row_array();

        if($receiptDetail["showdate"]!=1){
            $date = date_parse($receiptDetail["ReceiptDate"]);
            $year = $date["year"];
            $str_month = $arr_month[$date["month"]];
            $day = substr("00" . $date["day"], -2);
            }
            else{
                $year = '';
                $str_month = '';
                $day = '';
            }

        $settle_collect = array();
        $arrSettleField = array("SettleID1", "SettleID2", "SettleID3");
        foreach ($arrSettleField as $settleField) {
            $querySettleDetail = $this->receiptDb->query("SELECT TOP 1 PayType, PayAmount, convert(VARCHAR(20),PayDate,103) AS PayDate, ChequeNo, BankID, AccountID FROM SettleCollect WHERE ID=?"
                , array($receiptDetail[$settleField]));

            $settleDetail = $querySettleDetail->row_array();

            switch ($settleDetail['PayType']) {
                case 'transfer':
                    if (isset($settleDetail['AccountID']) && !empty($settleDetail['AccountID'])) {
                        $queryAccount = $this->receiptDb->query("SELECT tb1.AccountNo, tb2.NameT FROM AccountCollect tb1 LEFT JOIN BankCollect tb2 ON tb1.BankID = tb2.ID WHERE tb1.ID=?", array($settleDetail['AccountID']));
                        $resultAccount = $queryAccount->row_array();
                        $settleDetail["AccountNo"] = $resultAccount["AccountNo"];
                        $settleDetail["BankName"] = $resultAccount["NameT"];
                    }
                    break;
                case 'cheque':
                    if (isset($settleDetail["BankID"]) && !empty($settleDetail["BankID"])) {
                        $queryBank = $this->receiptDb->query("SELECT NameT FROM BankCollect WHERE ID=?", array($settleDetail['BankID']));
                        $resultBank = $queryBank->row_array();
                        $settleDetail["BankName"] = $resultBank["NameT"];
                    }
                    break;
            }
            array_push($settle_collect, $settleDetail);
        }

        $invoice_collect = array();
        $execFindInvoices = $this->receiptDb->query("findInvoiceCollect @id=?", array($uniqueReceiptId));
        if ($execFindInvoices)
            $invoice_collect = $execFindInvoices->result_array();

        if (sizeof($invoice_collect) <= 0)
            die('<br><h1>&nbsp;&nbsp;&nbsp;&nbsp;Invoice not provided.</h1>');
        $invoice = $invoice_collect[0];

        $queryPreviousInv = $this->receiptDb->query("findPreviouslyPayment @inv_id=?, @organ=?, @receipt_id=?", array($invoice['inv_id'], $receiptDetail['Organization'], $receiptDetail['ID']));
        $previously_payment = 0;
        $previously_receipt_qty = 0;
        foreach ($queryPreviousInv->result_array() as $previousInv) {
            $previously_payment = (+$previousInv["Paid"]) * (100 / 107);
            $previously_receipt_qty = $previousInv['ReceiptedQty'];
        }

        $currentSubPeriod = $previously_receipt_qty + 1;
        Utility::assign($po_no, $invoice['po_no']);
        Utility::assign($po_date, $invoice['po_date']);
        Utility::assign($ctr_id, $invoice["contract_id"]);
        Utility::assign($job_no, $invoice["job_id"]);
        Utility::assign($pay_amount, +$invoice["pay_amount"]);
        Utility::assign($vat, round(($pay_amount * (7 / 107)), 2));
        Utility::assign($amount, round($pay_amount - $vat, 2));

        $queryInvItem = $this->receiptDb->query("printinv_project_f @inv_id=?, @dtsource=?", array(trim($invoice['inv_id']), $receiptDetail['Organization']));
        $items_collect = $queryInvItem->result_array();
//        if (sizeof($items_collect) <= 0)
//            die('<br><h1>&nbsp;&nbsp;&nbsp;&nbsp;Item not provided.</h1>');

        Utility::assign($cmp_name_th, $companyDetail["NameT"]);
        Utility::assign($cmp_addr_th, $companyDetail["AddressT"] . " " . $companyDetail["CityT"] . " " . $companyDetail["Zipcode"]);
        Utility::assign($cmp_name_en, $companyDetail["NameE"]);
        Utility::assign($cmp_addr_en, $companyDetail["AddressE"] . " " . $companyDetail["CityE"] . " " . $companyDetail["Zipcode"]);
        Utility::assign($cmp_contract, "T: " . $companyDetail["Tel"] . "  F: " . $companyDetail["Fax"]);
        Utility::assign($cmp_tax_id, $companyDetail["TaxID"]);

        Utility::assign($receipt_id, Utility::ServiceIdBuilder($receiptDetail));
        Utility::assign($str_date, $day . "  " . $str_month . "  " . $year);
       
        Utility::assign($customer_name, $receiptDetail["CustomerName"]);
        Utility::assign($customer_address, $receiptDetail["CustomerAddress"]);
        Utility::assign($tax_id, $customerDetail["tax_id"]);
        Utility::assign($tax_extra, !empty($tax_id) ? $receiptDetail["TaxExtra"] : "");
        Utility::assign($term, $receiptDetail["Term"]);

        Utility::assign($total, round(+$invoice["amount"], 2));
        Utility::assign($balance, $total - $previously_payment);
        

        switch (mb_strtoupper($receiptDetail["Organization"])) {
            case "PGP":
                Utility::assign($annotation_extra, "\"บริษัท พรีไซซ กรีน โปรดักส์ จำกัด\" ธนาคารกสิกรไทย บัญชีกระแสรายวัน เลขที่ 470-1-02186-5 สาขาแจ้งวัฒนะ");
                break;
            case "PDE":
                Utility::assign($annotation_extra, "\"บริษัท พรีไซซ ดิจิตอล อีโคโนมี่ จำกัด\" ธนาคารกสิกรไทย บัญชีกระแสรายวัน เลขที่ 470-1-02186-5 สาขาแจ้งวัฒนะ");
                break;
            case "PEM":
                Utility::assign($annotation_extra, "\"บริษัท พรีไซซ อีเลคตริค แมนูแฟคเจอริ่ง จำกัด\" ธนาคารกสิกรไทย บัญชีกระแสรายวัน เลขที่ 183-1-06828-7 สาขารังสิต");
                break;
            case "PEM1":
                Utility::assign($annotation_extra, "\"บริษัท พรีไซซ อีเลคตริค แมนูแฟคเจอริ่ง จำกัด\" ธนาคารกสิกรไทย บัญชีกระแสรายวัน เลขที่ 470-1-03639-0 สาขาแจ้งวัฒนะ");
                break;
            case "PGS":
                Utility::assign($annotation_extra, "\"บริษัท พรีไซซ กรีน เทคโนโลยี แอนด์ เซอร์วิส จำกัด\" ธนาคารกรุงไทย บัญชีกระแสรายวัน เลขที่ 013-6-10490-8 สาขาถนนศรีอยุธยา");
                break;
            case "PSF":
                Utility::assign($annotation_extra, "\"บริษัท พรีไซซ สมาร์ท แฟคทอรี่ แอนด์ เซอร์วิส จำกัด\" ธนาคารกรุงไทย บัญชีกระแสรายวัน เลขที่ 013-6-10039-2 สาขาถนนศรีอยุธยา");
                break;
            default:
                Utility::assign($annotation_extra, "");
                break;
        }

        $pdf = new draftFormPDF();
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle($docType . '_' . $docCase);
        $pdf->setPrintHeader(true);
        $pdf->headerData = array(
            'cmp_name_th' => $cmp_name_th,
            'cmp_addr_th' => $cmp_addr_th,
            'cmp_name_en' => $cmp_name_en,
            'cmp_addr_en' => $cmp_addr_en,
            'cmp_contract' => $cmp_contract,
            'cmp_tax_id' => $cmp_tax_id,

            'receipt_id' => $receipt_id,
            'receipt_date' => $str_date,

            'customer_name' => $customer_name,
            'customer_addr' => $customer_address,
            'customer_tax_id' => $tax_id,
            'customer_tax_extra' => $tax_extra,
            'term' => $term,

            'po_no' => $po_no . "&nbsp;&nbsp;ลว." . $po_date,
            'job_no' => $job_no,
            'ctr_id' => $ctr_id,
        );
        $pdf->setHeaderData();
        $pdf->setPrintFooter(true);
        $pdf->setFooterData();

        $sumTotalAmount = 0;
        $rows_1 = "";
        foreach ($items_collect as $index => $item) {
            $sumTotalAmount += $item['Amount'];
            $item_no = $item['line'] > 0 ? $item['line'] : null;
            $item_desc = trim($item["des"]);
            $quantity = ($item['esr_aantal'] > 0 ? number_format($item['esr_aantal'], 2) : ($item['Qty'] > 0 ? number_format($item['Qty'], 2) : null)) . ' ' . (is_null($item['uom']) ? $item['unitcode'] : $item['uom']);
            $unit_price = $item["Price"] != 0 ? number_format($item["Price"], 2) : null;
            $item_amount = $item["Price"] != 0 ? number_format($item["Amount"], 2) : null;
            $row = '<tr>';
            $row .= '<td align="center" width="6%">' . $item_no . '</td>';
            $row .= '<td align="center" width="15%">' . $quantity . '</td>';
            $row .= '<td align="left" width="45%" colspan="2">' . nl2br(htmlentities($item_desc)) . '</td>';
            $row .= '<td align="right" width="17%">' . $unit_price . '</td>';
            $row .= '<td align="right" width="17%">' . $item_amount . '</td>';
            $row .= '</tr>';
            $rows_1 .= $row;
        }

        $rows_1 .= '<tr><td colspan="5"><br></td></tr>';
        if ($docCase !== 'normal' || $amount != $total) {
            $row = '<tr><td width="6%"></td><td width="15%"></td>';

            $row .= '<td width="30%" align="right">ยอดรวม (ก่อน VAT)</td>';
            $row .= '<td width="15%" align="right">' . number_format($sumTotalAmount, 2) . '</td>';
            $row .= '<td width="17%"></td>';
            $row .= '<td width="17%" align="right"></td></tr>';
            $rows_1 .= $row;
        }

        if ($docCase === 'advance') {
            $depositRate = (($total / $sumTotalAmount) * 100);
            $row = '<tr><td width="6%"></td><td width="15%"></td>';
            $row .= '<td width="30%" align="right">เรียกเก็บมัดจำ ' . number_format($depositRate, 0) . htmlentities('%') . ' (ก่อน VAT)</td>';
            $row .= '<td width="15%" align="right">' . number_format($total, 2) . '</td>';
            $row .= '<td width="17%"></td><td width="17%" align="right">';
            if ($amount == $total)
                $row .= number_format($total, 2);
            $row .= '</td></tr>';
            $rows_1 .= $row;
        } else if ($docCase === 'after') {
            $depositRate = round(($invoice['invADV'] / $sumTotalAmount) * 100);
            $row = '<tr><td width="6%"></td><td width="15%"></td>';
            $row .= '<td width="30%" align="right">หักมัดจำ ' . abs($depositRate) . htmlentities('%') . ' (ก่อน VAT)</td>';
            $row .= '<td width="15%" align="right">' . (!empty($invoice['invADV']) ? number_format(abs($invoice['invADV']), 2) : '') . '</td>';
            $row .= '<td width="17%"></td>';
            $row .= '<td width="17%" align="right"></td></tr>';
            $rows_1 .= $row;

            $row = '<tr><td width="6%"></td><td width="15%"></td>';
            $row .= '<td width="30%" align="right">เรียกเก็บเงินส่วนที่เหลือ ' . (($total / $sumTotalAmount) * 100) . htmlentities('%') . ' (ก่อน VAT)</td>';
            $row .= '<td width="15%" align="right">' . number_format($total, 2) . '</td>';
            $row .= '<td width="17%"></td><td width="17%" align="right">';
            if ($amount == $total)
                $row .= number_format($total, 2);
            $row .= '</td></tr>';
            $rows_1 .= $row;
        }
        if ($amount != $total) {
            $row = '<tr><td width="6%"></td>';
            $row .= '<td width="30%" align="right"><b>ชำระครั้งที่ ' . $currentSubPeriod . ' (ก่อน VAT)</b></td>';
            $row .= '<td width="15%" align="right">';
            $row .= '<table cellpadding="1.5"><tr><td style="border-bottom:  solid #000000;">' . number_format($amount, 2) . '</td></tr></table>';
            $row .= '</td>';
            $row .= '<td width="15%"></td><td width="17%"></td>';
            $row .= '<td width="17%" align="right">' . number_format($amount, 2) . '</td></tr>';
            $rows_1 .= $row;

            $row = '<tr><td width="6%"></td>';
            $row .= '<td width="30%" align="right">ยอดคงเหลือ (ก่อน VAT)</td>';
            $row .= '<td width="15%" align="right">';
            $row .= '<table cellpadding="1"><tr><td style="border-bottom:  solid #000000">';
            $row .= '<table cellpadding="0.5"><tr><td style="border-bottom:  solid #000000">' . number_format($balance - ($pay_amount - $vat), 2) . '</td></tr></table>';
            $row .= '</td></tr></table></td>';
            $row .= '<td width="15%"></td><td width="17%"></td><td width="17%"></td></tr>';
            $rows_1 .= $row;
        }

        $row = '<tr>';
        $row .= '<td width="6%"></td>';
        $row .= '<td width="15%"></td>';
        $addD = $receiptDetail['ReceiptCmpID']=='PEM1'?$receiptDetail['ReceiptCmpID'].'-D':'' ;
        $row .= '<td align="center" width="45%" colspan="2">' . htmlentities('** อ้างถึงใบแจ้งหนี้เลขที่ ' . $receiptDetail['ReceiptCmpID'] .$addD. trim($invoice['inv_id']) . ' **') . '</td>';
        
        $row .= '<td width="17%"></td>';
        $row .= '<td width="17%"></td>';
        $row .= '</tr>';
        $rows_1 .=  $row;

        $rows_2 = "";
        $mark = 'style="background-color: #000000"';
        foreach ($settle_collect as $index => $settle) {
            if ($index > 2)
                break;
            Utility::assign($settle_cash_mark, mb_ereg("cash", $settle["PayType"]) ? $mark : "");
            Utility::assign($settle_transfer_mark, mb_ereg("transfer", $settle["PayType"]) ? $mark : "");
            Utility::assign($settle_cheque_mark, mb_ereg("cheque", $settle["PayType"]) ? $mark : "");
            Utility::assign($settle_cheque, mb_ereg("cheque", $settle["PayType"]) ? $settle["ChequeNo"] : (mb_ereg("transfer", $settle["PayType"]) ? $settle["AccountNo"] : ""));
            Utility::assign($settle_bank, mb_ereg("(cheque|transfer)", $settle["PayType"]) ? $settle["BankName"] : (mb_ereg("cash", $settle["PayType"]) ? "" : ""));
            Utility::assign($settle_amount, mb_ereg("(cash|transfer|cheque)", $settle["PayType"]) ? (+$settle["PayAmount"] != 0 ? "=" . number_format($settle["PayAmount"], 2) : "") : "");
            Utility::assign($settle_date, mb_ereg("(cash|transfer|cheque)", $settle["PayType"]) ? $settle["PayDate"] : "");
            $row = <<<EOF
<tr>
    <td>
        
    </td>
</tr>
EOF;

            $rows_2 .= $row;
        }

        $pdf->footerData = array(
            'annotation_extra' => $annotation_extra,
            'rows_2' => $rows_2
        );
        $pdf->AddPage();
        $pdf->WriteHTML(
            '<table width="100%" border="0" cellpadding="3" cellspacing="0" style="line-height: 5mm;">' . $rows_1 . '</table>'
        );
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetXY(168, 220);
        $pdf->Cell(33, 12, number_format($amount, 2), 0, 1, 'R');
        $pdf->SetX(168);
        $pdf->Cell(33, 7.5, number_format($vat, 2), 0, 1, 'R');
        $pdf->Cell(132, 7.5, Utility::toThaiMoney($pay_amount), 0, 0, 'C');
        $pdf->SetX(168);
        $pdf->Cell(33, 12, number_format($pay_amount, 2), 0, 1, 'R');
        $pdf->Output();
    }
}