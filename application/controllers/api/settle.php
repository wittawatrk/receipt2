<?php

class settle extends CI_Controller
{
    public $receiptDb = null;

    public function __construct()
    {
        parent::__construct();
        $this->receiptDb = $this->load->database('receipt', true);
    }

    public function get_settleDetail($settleId)
    {

        $recQuerySettle = $this->receiptDb->query("SELECT ID, PayType, PayDate, PayAmount, ChequeNo, AccountID, BankID FROM SettleCollect WHERE ID=?"
            , array($settleId));

        if ($recQuerySettle) {
            $settleDetail = array();
            if ($recQuerySettle->num_rows() > 0) {
                $result = $recQuerySettle->row_array();
                $settleDetail = array(
                    'id' => $result['ID'],
                    'bankId' => $result['BankID'],
                    'accountId' => $result['AccountID'],
                    'chequeNo' => $result['ChequeNo'],
                    'payType' => $result['PayType'],
                    'payDate' => $result['PayDate'],
                    'payAmount' => $result['PayAmount'] . ""

                );
            }
            header('content-type: application/json');
            echo json_encode($settleDetail);
        }
    }
}