<?php

class invoice extends CI_Controller
{

    public $receiptDb = null;
    public $userInfo = null;

    public function __construct()
    {
        parent::__construct();

        $this->receiptDb = $this->load->database('receipt', true);
        $this->userInfo = $this->session->userInfo;
    }

    public function get_invoiceList($receiptUniqueId)
    {
        $recQueryInvoices = $this->receiptDb->query("findInvoiceCollect @id=?"
            , array($receiptUniqueId));
        if ($recQueryInvoices) {
            header('content-type: application/json');
            echo json_encode($recQueryInvoices->result_array());
        }
    }

    public function get_invoiceDetail($cmpCode, $invId)
    {
        $recQueryInvDetail = $this->receiptDb->query("invoiceDetail @dtsource=?, @id=?", array($cmpCode, $invId));
        if ($recQueryInvDetail) {
            header('content-type: application/json');
            echo json_encode($recQueryInvDetail->row_array());
        }
    }

    public function get_asAutoComplete($cmpCode, $customerId)
    {
        $recQueryInvoices = $this->receiptDb->query("findInvoiceAutocomplete @CustomerID=?, @dtsource=?", array($customerId, $cmpCode));
        if ($recQueryInvoices) {
            $invoices = array();
            foreach ($recQueryInvoices->result_array() as $invoice)
                array_push($invoices, array("value" => $invoice["InvID"], "data" => $invoice));

            header('content-type: application/json');
            echo json_encode($invoices);
        }
    }

    public function get_paid($cmpCode, $invId, $receiptUniqueId)
    {
        $recQueryPaid = $this->receiptDb->query("SELECT isnull(sum(Paid),0) AS paid FROM InvoiceCollect WHERE InvoiceID=? AND ReceiptCollectID IN (SELECT ID FROM ReceiptCollect WHERE Cancel=0 AND ID!=? AND Organization=?)"
            , array($invId, $receiptUniqueId, $cmpCode));
        if ($recQueryPaid) {
            header('content-type: application/json');
            echo json_encode($recQueryPaid->row_array());
        }
    }
}