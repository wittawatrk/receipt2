<?php

class company extends CI_Controller
{
    public $assetDb = null;
    public $userInfo = array();

    public function __construct()
    {
        parent::__construct();
        $this->assetDb = $this->load->database('asset', true);
        $this->userInfo = $this->session->userInfo;
        $this->userInfo['CompanyAllowed'] = Utility::RelatedCompanyBuilder($this->userInfo['CompanyAllowed'], $this->userInfo['CompanyCode']);
    }

    public function related()
    {
        $relatedCompany = array();
        $recQueryCompany = $this->assetDb->query('SELECT ShortName, NameE AS LongName FROM company WHERE ShortName IS NOT NULL AND ShortName IN ? ORDER BY ShortName ASC'
            , array($this->userInfo['CompanyAllowed']));
        if ($recQueryCompany)
            $relatedCompany = $recQueryCompany->result_array();
        header('content-type: application/json');
        echo json_encode($relatedCompany);
    }
}