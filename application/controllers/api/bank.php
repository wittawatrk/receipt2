<?php

/**
 * Created by PhpStorm.
 * User: Chanon
 * Date: 16/08/2017
 * Time: 13:30
 */
class bank extends CI_Controller
{
    public $receiptDb = null;

    public function __construct()
    {
        parent::__construct();
        $this->receiptDb = $this->load->database('receipt', true);
    }

    public function get_asAutoComplete()
    {
        $recQueryBanks = $this->receiptDb->query("select ID, NameT from BankCollect where Show=1");
        if($recQueryBanks){
            header('content-type: application/json');
            echo json_encode($recQueryBanks->result_array());
        }
    }
}