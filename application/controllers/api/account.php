<?php

class account extends CI_Controller
{
    public $receiptDb = null;
    public $userInfo = null;

    public function __construct()
    {
        parent::__construct();
        $this->receiptDb = $this->load->database('receipt', true);
        $this->userInfo = $this->session->userInfo;
    }

    public function get_asAutoComplete($cmpCode)
    {
        $recQueryAccounts = $this->receiptDb->query("SELECT tb2.ID, tb1.NameT, tb1.NameE, tb2.AccountNo, tb2.ShortName FROM BankCollect tb1 INNER JOIN AccountCollect tb2 ON tb1.ID=tb2.BankID WHERE tb2.Organization=? AND tb1.Show=1 ORDER BY tb2.ShortName"
            , array($cmpCode));
        if ($recQueryAccounts) {
            header('content-type: application/json');
            echo json_encode($recQueryAccounts->result_array());
        }
    }
}