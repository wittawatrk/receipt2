<?php

class customer extends CI_Controller
{

    public $receiptDb = null;

    public function __construct()
    {
        parent::__construct();

        $this->receiptDb = $this->load->database('receipt', true);
        $this->userInfo = $this->session->userInfo;
    }

    public function get_customers($cmpCode)
    {
        $recQueryCustomer = $this->receiptDb->query("findCustomerAutocomplete @dtsource=?"
            , array($cmpCode));
        if ($recQueryCustomer) {
            $data = array();
            foreach ($recQueryCustomer->result_array() as $row) {
                array_push($data, array("data" => $row["ID"], "value" => $row["CustomerName"]));
            }
            header('content-type: application/json');
            echo json_encode($data);
        }
    }
    public function get_customersearch($cmp){
        $recQueryCustomer = $this->receiptDb->query("select '['+CustomerID+']  '+convert(varchar(max),customerName) as fullname ,customerName as name,CustomerID as ID from receiptCollect where CustomerID is not null ");
    if ($recQueryCustomer) {
        $data = array();
        foreach ($recQueryCustomer->result_array() as $row) {
            array_push($data, array("data" => $row["name"],"value" => $row["fullname"]));
        }
        header('content-type: application/json');
        echo json_encode($data);
    }
    }
    public function get_customerDetail($cmpCode, $customerId)
    {
        $recQueryCustomer = $this->receiptDb->query("findCustomerDetail @id=?, @dtsource=?"
            , array($customerId, $cmpCode));
        if ($recQueryCustomer && $recQueryCustomer->num_rows() > 0) {
            $result = $recQueryCustomer->row_array();
            $recQueryCustomer->free_result();

            $response["id"] = $customerId;
            $response["RawCustomerID"] = $result["ID"];
            $response["CustomerID"] = $result["debcode"];
            $response["CustomerName"] = $result["CustomerName"];
            $response["CustomerAddress"] = $result["CustomerAddress"];
            $response["CustomerTaxID"] = $result["tax_id"];

            header('content-type: application/json');
            echo json_encode($response);
        }
    }
}