<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class form extends CI_Controller
{
    

    public $receiptDb = null;
    public $userInfo = array();

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");
        $this->receiptDb = $this->load->database('receipt', true);
        $this->userInfo = $this->session->userInfo;
        $this->userInfo['CompanyAllowed'] = Utility::RelatedCompanyBuilder($this->userInfo['CompanyAllowed'], $this->userInfo['CompanyCode']);
    }

    public function getform($type)
    {
        $queryReceipt = $this->receiptDb->query("SELECT id,name FROM printform WHERE type=? "
            , array($type));
        if ($queryReceipt && $queryReceipt->num_rows() > 0) {
            $receipt = $queryReceipt->result_array();
            header('content-type: application/json');
            echo json_encode($receipt);
            
            $queryReceipt->free_result();

        }}
        
  
        
            
               

                    
                
               
        
        

  
}