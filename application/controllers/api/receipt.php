<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receipt extends CI_Controller
{
    

    public $receiptDb = null;
    public $userInfo = array();

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");
        $this->receiptDb = $this->load->database('receipt', true);
        $this->userInfo = $this->session->userInfo;
        $this->userInfo['CompanyAllowed'] = Utility::RelatedCompanyBuilder($this->userInfo['CompanyAllowed'], $this->userInfo['CompanyCode']);
    }

    public function get_receiptDetail($id)
    {
        $queryReceipt = $this->receiptDb->query("SELECT * FROM ReceiptCollect WHERE id=? AND Cancel = 0"
            , array($id));
        if ($queryReceipt && $queryReceipt->num_rows() > 0) {
            $receipt = $queryReceipt->row_array();
            $queryReceipt->free_result();

            $assetDb = $this->load->database('asset', true);
            $queryCmpName = $assetDb->query("SELECT TOP 1 replace(ShortName+' - '+ NameT, '  ', '') AS Name FROM company WHERE ShortName=?"
                , array($receipt['Organization']));
                $queryForm =  $this->receiptDb->query("SELECT *  FROM printForm WHERE id=? ",
                array($receipt['formid']));
               if($queryForm->num_rows()>0){
                   $form = $queryForm->row_array();
                   $queryForm->free_result();
               }
               else $form = null;
            if ($queryCmpName) {
                $response = array(
                    "id" => $receipt['ID'],
                    "receiptId" => $receipt['ReceiptType'] == 'product' ? Utility::IdBuilder($receipt) :$receipt['ReceiptType'] == 'service' ? Utility::ServiceIdBuilder($receipt):Utility::ServiceNovatIdBuilder($receipt),
                    "receiptType" => $receipt['ReceiptType'],
                    "receiptCase" => $receipt['ReceiptCase'],
                    "receiptDate" => $receipt['ReceiptDate'],
                    "organization" => $receipt['Organization'],
                    "organizationName" => $queryCmpName->row_array()['Name'],
                    "rawCustomerId" => $receipt['RawCustomerID'],
                    "customerId" => $receipt['CustomerID'],
                    "customerName" => $receipt['CustomerName'],
                    "customerAddress" => $receipt['CustomerAddress'],
                    "taxExtra" => $receipt['TaxExtra'],
                    "formid" => $receipt['formid'],
                    "term" => $receipt['Term'],
                    "remark" => $receipt['Remark'],
                    "docurl" =>$form['doc_url'],
                    "showdate" =>$receipt['showdate']?$receipt['showdate']:'0',
                    "settleId" => array($receipt['SettleID1'], $receipt['SettleID2'], $receipt['SettleID3'])
                );
                header('content-type: application/json');
                echo json_encode($response);
            }
        }
    }

    public function get_receiptAll($receiptType)
    {    $_POST = json_decode($this->input->raw_input_stream, true);
      
        $post=$this->input->post();
        $query = array();
        foreach (['customer','company','receiptid', 'invoice','customer'] as $key) {
            $query[$key] = isset($post[$key]) && !is_null($post[$key]) && !empty($post[$key]) ? $post[$key] : null;
        }
        
        foreach (['startDate', 'endDate'] as $key) {
            $query[$key] = isset($post[$key]) && !is_null($post[$key]) && !empty($post[$key]) ? date('Y-m-d',+$post[$key]/1000) : null;
        }
            $allow="";
        foreach($this->userInfo['CompanyAllowed']as $val){
            $allow.= $val.",";
        }
       
        $response = array("state" => false);
        $queryReceipts = $this->receiptDb->query("seachlist @invoiceNo=? ,@customer=?,@startDate=?,@endDate=?,@type=? ,@receiptid=?,@company=?,@role=?",array($query['invoice'],$query['customer'],$query['startDate'],$query['endDate'],$receiptType,$query['receiptid'],$query['company'],$allow));
        // $queryReceipts = $this->receiptDb->query("SELECT ID, ReceiptCmpID,ReceiptYearID,ReceiptMonthID,ReceiptRunning,CustomerName,ReceiptDate,Revision, Cancel, Reason, ReceiptType ,formid FROM ReceiptCollect WHERE Show=1 AND ReceiptType=? AND Organization IN ? ORDER BY CreateDate DESC"
         
        //, array($receiptType,$this->userInfo['CompanyAllowed']));

        if ($queryReceipts) {
            $receiptCollect = array();
            foreach ($queryReceipts->result_array() as $receiptObj) {
                $receiptID = $receiptType == 'product' ? Utility::IdBuilder($receiptObj) :$receiptType == 'service' ? Utility::ServiceIdBuilder($receiptObj):Utility::ServiceNovatIdBuilder($receiptObj);

                $queryInvoices = $this->receiptDb->query("SELECT InvoiceID AS inv_id, ContractID AS contract_id, Paid AS paid FROM InvoiceCollect WHERE ReceiptCollectID=? ORDER BY [Order] ASC"
                    , array($receiptObj['ID']));
                $queryFormtype =  $this->receiptDb->query("SELECT doc_url as DocUrl FROM printForm WHERE id=? ",
                array($receiptObj['formid']));
               if($queryFormtype->num_rows()>0){
                foreach ($queryFormtype->result()as $val){
                    $docurl = $val->DocUrl;
                }
            }
            else $docurl = null;
               
            
                if ($queryInvoices) {
                    array_push($receiptCollect, array(
                        "id" => $receiptObj["ID"],
                        "receiptID" => $receiptID,
                        "customerName" => trim($receiptObj["CustomerName"]),
                        "receiptDate" => !empty($receiptObj["ReceiptDate"]) ? date_format(date_create($receiptObj["ReceiptDate"]), "d/m/Y") : null,
                        "revision" => +$receiptObj["Revision"],
                        "cancel" => +$receiptObj["Cancel"],
                        "reason" => $receiptObj["Reason"],
                        "inv_collect" => $queryInvoices->result_array(),
                        "receipt_type" => $receiptObj["ReceiptType"],
                        "docurl" => $docurl==null?$receiptObj["ReceiptType"]:$docurl,
                    ));
                }
            }
            $response['data'] = $receiptCollect;
            $response['state'] = true;
            $response['allow'] = $allow;
           
         
            header('content-type: application/json', true);
            echo json_encode($response);
        }
    }

    public function get_availableId($receiptType, $cmpCode)
    {
        $running = 1;
        $newReceiptId = "";
        $queryAvailableId = $this->receiptDb->query("SELECT TOP 1 ReceiptRunning FROM ReceiptCollect WHERE Cancel=0 AND ReceiptCmpID=? AND ReceiptYearID=(year(getdate())%100) AND ReceiptMonthID=month(getdate()) AND ReceiptType=? ORDER BY ReceiptRunning DESC"
            , array($cmpCode, $receiptType));

        if ($queryAvailableId) {
            if ($queryAvailableId->num_rows() > 0) {
                $running = ++$queryAvailableId->row_array()['ReceiptRunning'];
            }
            $arrAsset = array(
                "ReceiptCmpID" => $cmpCode,
                "ReceiptYearID" => date("Y", time()),
                "ReceiptMonthID" => date("m", time()),
                "ReceiptRunning" => $running);
            switch ($receiptType) {
                case "product":
                    $newReceiptId = Utility::IdBuilder($arrAsset);
                    break;
                case "service":
                    $newReceiptId = Utility::ServiceIdBuilder($arrAsset);
                    break;
                case "service_novat":
                $newReceiptId = Utility::ServiceNovatIdBuilder($arrAsset);
                    break;
            }
            header('content-type: application/json');
            echo json_encode(array('receiptId' => $newReceiptId, 'a' => $arrAsset));
        }
    }

    public function get_verifyReceiptId($receiptType, $cmpCode, $customId)
    {
        $isExist = true;

        $str_id_compare = "'-'";
        $tempReceiptId = "";
        $arrAsset = array(
            "ReceiptCmpID" => strtoupper($cmpCode),
            "ReceiptYearID" => date("y", time()),
            "ReceiptMonthID" => date("m", time()),
            "ReceiptRunning" => 1,
            "ReceiptCustomID" => $customId);
        switch ($receiptType) {
            case "product":
                $str_id_compare = "replace('RC-'+ReceiptCmpID+'-'+right('00'+cast(ReceiptYearID as varchar(2)),2)+'-'+right('00'+cast(ReceiptMonthID as varchar(2)),2)+'-'+right('000'+cast(ReceiptRunning as varchar(3)),3),' ','')";
                $tempReceiptId = "RC-" . strtoupper($cmpCode) . "-" . date("y", time()) . "-" . $customId;
                $receiptId = Utility::IdBuilder($arrAsset);
                break;
            case "service":
                $str_id_compare = "replace(ReceiptCmpID+'-S'+right('00'+cast(ReceiptYearID as varchar(2)),2)+right('00'+cast(ReceiptMonthID as varchar(2)),2)+right('000'+cast(ReceiptRunning as varchar(3)),3),' ','')";
                $tempReceiptId = strtoupper($cmpCode) . "-S" . date("y", time()) . $customId;
                $receiptId = Utility::ServiceIdBuilder($arrAsset);
                break;
                case "service_novat":
                $str_id_compare = "replace(ReceiptCmpID+'-'+right('00'+cast(ReceiptYearID as varchar(2)),2)+right('00'+cast(ReceiptMonthID as varchar(2)),2)+right('000'+cast(ReceiptRunning as varchar(3)),3),' ','')";
                $tempReceiptId = strtoupper($cmpCode) . "-" . date("y", time()) . $customId;
                $receiptId = Utility::ServiceNOvatIdBuilder($arrAsset);
                break;
        }

        $queryID = $this->receiptDb->query("SELECT ID FROM ReceiptCollect WHERE Cancel = 0 AND " . $str_id_compare . "=replace(?,' ','') AND Organization=? AND ReceiptType=?"
            , array($tempReceiptId, $cmpCode, $receiptType));

        if ($queryID) {
            if ($queryID->num_rows() == 0)
                $isExist = false;

            header('content-type: application/json');
            echo json_encode(array('alreadyExist' => $isExist, 'receiptId' => $receiptId));
        }
    }

    public function post_newReceipt($receiptType, $cmpCode, $receiptId)
    {
        $isExist = true;

        // ตรวจสอบว่ามี ReceiptID ตรงกันหรือไม่
        $str_id_compare = "'-'";
        switch ($receiptType) {
            case "product":
                $str_id_compare = "replace('RC-'+ReceiptCmpID+'-'+right('00'+cast(ReceiptYearID as varchar(2)),2)+'-'+right('00'+cast(ReceiptMonthID as varchar(2)),2)+'-'+right('000'+cast(ReceiptRunning as varchar(3)),3),' ','')";
                break;
            case "service":
                $str_id_compare = "replace(ReceiptCmpID+'-S'+right('00'+cast(ReceiptYearID as varchar(2)),2)+right('00'+cast(ReceiptMonthID as varchar(2)),2)+right('000'+cast(ReceiptRunning as varchar(3)),3),' ','')";
                break;
            case "service_novat":
                $str_id_compare = "replace(ReceiptCmpID+'-'+right('00'+cast(ReceiptYearID as varchar(2)),2)+right('00'+cast(ReceiptMonthID as varchar(2)),2)+right('000'+cast(ReceiptRunning as varchar(3)),3),' ','')";
                break;
        }

        $queryID = $this->receiptDb->query("SELECT ID FROM ReceiptCollect WHERE Cancel = 0 AND " . $str_id_compare . "=replace(?,' ','') AND Organization=? AND ReceiptType=?"
            , array($receiptId, $cmpCode, $receiptType));

        if ($queryID && $queryID->num_rows() > 0) {
            // เมื่อพบ ReceiptID ตรงกันอยู่ ให้คืนค่า state เท่ากับ false
            header('content-type: application/json');
            echo json_encode(array('state' => false, 'alreadyExist' => $isExist));
            die();
        } else {

            // เมื่อไม่มี ReceiptID ตรงกัน เริ่มทำการบันทึก ReceiptID ใหม่ลงบนฐานข้อมูล
            $sReceiptID = explode("-", $receiptId);
            if ($receiptType == "service"||$receiptType =="service_novat") {
                $decrease = !(mb_eregi("^pem$", $sReceiptID[0])) ? 0 : 43;
                $sReceiptID[1] = $sReceiptID[0];
                $sReceiptID[2] = substr("00" . (+mb_substr($receiptId, -7, 2)) - $decrease, -2);
                $sReceiptID[3] = mb_substr($receiptId, -5, 2);
                $sReceiptID[4] = mb_substr($receiptId, -3, 3);
                $sReceiptID[0] = "RC";
            }
            if (count($sReceiptID) != 5 || $sReceiptID[1] != $cmpCode) {
                
                HttpResponse::code(500);
                die();
            }

            // สร้าง unique identity
            $uniqueId = "";
            $queryUniqueId = $this->receiptDb->query("select newid() as newId");
            if ($queryUniqueId)
                $uniqueId = $queryUniqueId->row_array()['newId'];

            $statementInsert = $this->receiptDb->query("INSERT INTO ReceiptCollect (ID,ReceiptCmpID,ReceiptYearID,ReceiptMonthID,ReceiptRunning,Organization,CreateBy,ReceiptType,ReceiptNo) " .
                "VALUES (?,?,?,?,?,?,?,?,?)"
                , array($uniqueId,
                    $sReceiptID[1],
                    $sReceiptID[2],
                    $sReceiptID[3],
                    $sReceiptID[4],
                    $cmpCode,
                    $this->userInfo['CitizenID'],
                    $receiptType,
                    $receiptId));
            if ($statementInsert) {
                for ($i = 1; $i <= 3; $i++) {
                    $this->receiptDb->query("INSERT INTO SettleCollect (ID,PayType) SELECT SettleID" . $i . ", '' FROM ReceiptCollect WHERE ID=?"
                        , array($uniqueId));
                }
                header('content-type: application/json');
                echo json_encode(array("state" => true, "uniqueId" => $uniqueId));
            }
        }
    }

    public function delete_receipt($receiptId)
    {
        $reason = $_GET['reason'];

        $this->receiptDb->trans_start();
        $this->receiptDb->query("UPDATE ReceiptCollect SET Show=0, Cancel=1, CancelDate=GETDATE(), CancelBy=?, Reason=? WHERE ID=?"
            , array($this->userInfo['CitizenID'], $reason, $receiptId));
        $this->receiptDb->trans_complete();

        if ($this->receiptDb->trans_status() === false) {
            HttpResp::code(500);
            die();
        }
        header('content-type: application/json');
        echo json_encode(array("state" => $this->receiptDb->trans_status()));
    }


    public function post_customerToReceipt($uniqueReceiptId)
    {
        $_POST = json_decode($this->input->raw_input_stream, true);
        $post = $this->input->post();
        
        $receipt_date = $post["receipt_date"]?date("Y-m-d", round((+$post["receipt_date"]) / 1000)):null; 
        $customer_id = $post["customer_id"];
        $customer_name = $post["customer_name"];
        $customer_address = $post["customer_address"];
        $raw_customer_id = $post["raw_customer_id"];
        $tax_extra = isset($post["tax_extra"]) ? $post["tax_extra"] : null;
     /*   if (!$receipt_date){
            $insertCustomer = $this->receiptDb->query("UPDATE ReceiptCollect SET CustomerID=?, CustomerName=?, CustomerAddress=?,  RawCustomerID=?, TaxExtra=?, Revision=(Revision+1), ModifyBy=?, ModifyDate=GETDATE() WHERE ID=?"
            , array($customer_id, $customer_name, $customer_address, $raw_customer_id, $tax_extra, $this->userInfo["CitizenID"], $uniqueReceiptId));

        }
        else*/
         $insertCustomer = $this->receiptDb->query("UPDATE ReceiptCollect SET CustomerID=?, CustomerName=?, CustomerAddress=?, ReceiptDate=?, RawCustomerID=?, TaxExtra=?, Revision=(Revision+1), ModifyBy=?, ModifyDate=GETDATE() WHERE ID=?"
            , array($customer_id, $customer_name, $customer_address, $receipt_date, $raw_customer_id, $tax_extra, $this->userInfo["CitizenID"], $uniqueReceiptId));

        if ($insertCustomer) {
            header('Content-Type: application/json');
            echo json_encode(array("state" => true, "id" => $uniqueReceiptId));
        }
    }

    public function patch_receiptDetail($uniqueReceiptId)
    {
        $_POST = json_decode($this->input->raw_input_stream, true);
        $post = $this->input->post();
        $queryReceiptDetail = $this->receiptDb->query("SELECT * FROM ReceiptCollect WHERE ID=?", array(trim($uniqueReceiptId)));
        if ($queryReceiptDetail && $queryReceiptDetail->num_rows() > 0) {
            $id = trim($uniqueReceiptId);
            $receipt_date = $post["receiptDate"]?date("Y-m-d", round((+$post["receiptDate"]) / 1000)):null; 
            $this->receiptDb->trans_start();
            $this->receiptDb->query("UPDATE ReceiptCollect SET CustomerName=?, CustomerAddress=?, TaxExtra=?, Term=?, Remark=?, ReceiptCase=?, ModifyBy=?, ModifyDate=getdate(),ReceiptDate=?,formid=?, showdate=?,Revision=Revision+1 WHERE ID=?"
                , array($post['customerName'], $post['customerAddress'], $post['taxExtra'], $post['term'], $post['remark'], $post['receiptCase'], $this->userInfo['CitizenID'], $receipt_date,$post['formid'],$post['showdate'],$id));

            foreach ($post["settles"] as $settle) {
                $pay_type = "";
                $pay_amount = null;
                $pay_date = null;
                $cheque_no = null;
                $bank_id = null;
                $account_id = null;

                if (isset($settle["payType"]) && !empty($settle["payType"])) {
                    $pay_type = $settle["payType"];
                    if (!empty($settle["payAmount"]))
                        $pay_amount = mb_eregi_replace(",", "", $settle["payAmount"]);
                    if (!empty($settle["payDateTime"]))
                        $pay_date = date("Y-m-d", $settle["payDateTime"] / 1000);

                    if ($pay_type == "transfer" && !empty($settle["accountId"])) {
                        $account_id = $settle["accountId"];
                    } else if ($pay_type == "cheque") {
                        if (!empty($settle["chequeNo"]))
                            $cheque_no = $settle["chequeNo"];
                        if (!empty($settle["bankId"]))
                            $bank_id = $settle["bankId"];
                    }
                }

                $this->receiptDb->query("UPDATE SettleCollect SET PayType=?, PayAmount=?, PayDate=?, ChequeNo=?, BankID=?, AccountID=? WHERE ID=?"
                    , array($pay_type, $pay_amount, $pay_date, $cheque_no, $bank_id, $account_id, $settle["id"]));
            }

            $delInvoiceOlder = $this->receiptDb->query("DELETE FROM InvoiceCollect WHERE ReceiptCollectID=?", array($id));
            if ($delInvoiceOlder) {
                $invoiceCollect = isset($post["invoiceCollect"]) && !empty($post["invoiceCollect"]) ? $post["invoiceCollect"] : array();
                foreach ($invoiceCollect as $key => $invoice) {
                    $insertInvoice = $this->receiptDb->query("INSERT INTO InvoiceCollect (InvoiceID, ContractID, Paid, ReceiptCollectID, [Order]) VALUES (?, ?, ?, ?, ?)"
                        , array(trim($invoice["inv_id"]), trim($invoice["contract_id"]), mb_eregi_replace(",", "", trim($invoice["pay_amount"])), $id, $key));
                }
            }
            $this->receiptDb->trans_complete();
            echo json_encode(array('state' => $this->receiptDb->trans_status()));
        }
    }
}