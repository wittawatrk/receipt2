<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['dist'] = 'home';
$route['dist/(.+)'] = 'home';

$route['docs/(:any)/(:any)']['get'] = 'docs/$1/index/$2/$1';

$route['api/receipt/customer/(:any)']['post'] = 'api/receipt/post_customerToReceipt/$1';
$route['api/receipt/(:any)']['delete'] = 'api/receipt/delete_receipt/$1';
$route['api/receipt/(:any)']['get'] = 'api/receipt/get_receiptDetail/$1';

$route['api/receipt/(:any)']['patch'] = 'api/receipt/patch_receiptDetail/$1';
$route['api/receipt/(:any)/([a-zA-Z0-9]+)/availableId']['get'] = 'api/receipt/get_availableId/$1/$2';
$route['api/receipt/(:any)/([a-zA-Z0-9]+)/verifyReceiptId/(:any)']['get'] = 'api/receipt/get_verifyReceiptId/$1/$2/$3';
$route['api/receipt/(:any)/([a-zA-Z0-9]+)/(:any)']['post'] = 'api/receipt/post_newReceipt/$1/$2/$3';
$route['api/receipts/(:any)']['post'] = 'api/receipt/get_receiptAll/$1';

$route['api/customers/(:any)']['get'] = 'api/customer/get_customers/$1';
$route['api/customer/([a-zA-Z0-9]+)/(:num)']['get'] = 'api/customer/get_customerDetail/$1/$2';
$route['api/customersearch/(:any)']['get'] = 'api/customer/get_customersearch/$1';
$route['api/settle/(:any)']['get'] = 'api/settle/get_settleDetail/$1';

$route['api/invoices/(:any)']['get'] = 'api/invoice/get_invoiceList/$1';
$route['api/invoice/(:any)/(:any)']['get'] = 'api/invoice/get_invoiceDetail/$1/$2';
$route['api/invoice/autoComplete/(:any)/(:any)']['get'] = 'api/invoice/get_asAutoComplete/$1/$2';
$route['api/invoice/paid/(:any)/(:any)/(:any)']['get'] = 'api/invoice/get_paid/$1/$2/$3';

$route['api/bank/autoComplete']['get'] = 'api/bank/get_asAutoComplete';

$route['api/account/autoComplete/(:any)']['get'] = 'api/account/get_asAutoComplete/$1';

$route['api/form/(:any)']['get'] = 'api/form/getform/$1';