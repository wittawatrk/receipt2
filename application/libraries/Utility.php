<?php

class Utility
{
    public static function IdBuilder($asset = array())
    {
        $receiptCmpID = trim($asset["ReceiptCmpID"]);
        $receiptYearID = substr("00". $asset["ReceiptYearID"], -2);
        $receiptMonthID = substr("00". $asset["ReceiptMonthID"], -2);
        $receiptRunning = "-" . substr("000". $asset["ReceiptRunning"], -3);
        if (!empty($asset["ReceiptCustomID"])) {
            $receiptMonthID = "";
            $receiptRunning = trim($asset["ReceiptCustomID"]);
        }
        return str_replace(" ", "", "RC-" . $receiptCmpID . "-" . $receiptYearID . "-" . $receiptMonthID . $receiptRunning);
    }

    public static function RelatedCompanyBuilder($arrCmp, $curCmp)
    {
        array_unshift($arrCmp, $curCmp);
        return array_values(array_unique($arrCmp));
    }

    public static function assign(&$variable, $str = "")
    {
        if (isset($str) && !empty($str) && !is_null($str)) {
            $variable = $str;
            return true;
        }
        $variable = "";
        return false;
    }


    public static function makeDot($num = 1)
    {
        $t = "";
        for ($i = 0; $i < $num; $i++) {
            $t .= ".";
        }
        return $t;
    }

    public static function addComma($num)
    {
        return number_format($num, 2, ".", ",");
    }

    public static function toThaiMoney($number)
    {
        if ($number == 0) {
            return "ศูนย์บาทถ้วน";
        }
        $number = round($number, 2);
        $wNum = array('', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
        $wSize = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
        $number = str_replace(",", "", $number);
        $number = str_replace(" ", "", $number);
        $number = str_replace("บาท", "", $number);
        $numbers = explode(".", $number);
        if (sizeof($numbers) > 2) {
            return 'จำนวนทศนิยมมากเกินไป';
        }
        $convert = "";
        $c = 0;
        $strlen = strlen($numbers[0]);
        for ($i = 0; $i < $strlen; $i++) {
            $num = substr($numbers[0], $strlen - $i - 1, 1);
            if ($num == "0") continue;
            if ($i >= $c + sizeof($wSize))
                $c = $c + sizeof($wSize) - 1;
            $tS = ($wSize[$i - $c]);
            $tN = ($wNum[$num]);
            $convert = $tN . $tS . $convert;
        }
        $convert .= "บาท";
        if (!isset($numbers[1]) || $numbers[1] == 0 || $numbers[1] == '0' || $numbers[1] == '00') {
            $convert .= "ถ้วน";
            return $convert;
        }
        if (strlen($numbers[1]) < 2) {
            $numbers[1] .= "0";
        }
        $strlen2 = strlen($numbers[1]);
        $convert2 = "";
        for ($i = 0; $i < $strlen2; $i++) {
            $num = substr($numbers[1], $strlen2 - $i - 1, 1);
            if ($num == "0") continue;
            $tS = ($wSize[$i]);
            $tN = ($wNum[$num]);
            $convert2 = $tN . $tS . $convert2;
        }
        $convert2 .= "สตางค์";
        $convert .= $convert2;
        $convert = str_replace("หนึ่งสิบ", "สิบ", $convert);
        $convert = str_replace("สองสิบ", "ยี่สิบ", $convert);
        $convert = str_replace("สิบหนึ่ง", "สิบเอ็ด", $convert);
        return $convert;
    }

    public static function ServiceIdBuilder($asset = array())
    {
        $receiptCmpID = trim($asset["ReceiptCmpID"]);
        $receiptYearID = substr("00" . $asset["ReceiptYearID"], -2);
        if (mb_eregi("^pem$", $receiptCmpID))
            $receiptYearID = substr("00" . (+$asset["ReceiptYearID"] + 43), -2);

        $receiptMonthID = substr("00" . +$asset["ReceiptMonthID"], -2);
        $receiptRunning = substr("000" . +$asset["ReceiptRunning"], -3);
        if (!empty($asset["ReceiptCustomID"])) {
            $receiptMonthID = "";
            $receiptRunning = substr("00000" . +$asset["ReceiptCustomID"], -5);
        }
        return str_replace(" ", "", $receiptCmpID . "-S" . $receiptYearID . $receiptMonthID . $receiptRunning);
    }

    public static function ServiceNovatIdBuilder($asset = array())
    {
        $receiptCmpID = trim($asset["ReceiptCmpID"]);
        $receiptYearID = substr("00" . $asset["ReceiptYearID"], -2);
        if (mb_eregi("^pem$", $receiptCmpID))
            $receiptYearID = substr("00" . (+$asset["ReceiptYearID"] + 43), -2);

        $receiptMonthID = substr("00" . +$asset["ReceiptMonthID"], -2);
        $receiptRunning = substr("000" . +$asset["ReceiptRunning"], -3);
        if (!empty($asset["ReceiptCustomID"])) {
            $receiptMonthID = "";
            $receiptRunning = substr("00000" . +$asset["ReceiptCustomID"], -5);
        }
        return str_replace(" ", "", $receiptCmpID . "-" . $receiptYearID . $receiptMonthID . $receiptRunning);
    }
}