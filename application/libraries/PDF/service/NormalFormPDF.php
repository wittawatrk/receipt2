<?php
if (!file_exists(APPPATH . 'libraries/MapVal.php'))
    include(APPPATH . 'libraries/MapVal.php');

class NormalFormPDF extends PDF
{
    public $headerData = array();
    public $footerData = array();

    public function __construct()
    {
        parent::__construct();
        $this->setHeaderMargin(15);
        $this->setFooterMargin(100);
        $this->SetTopMargin(110);
        $this->SetAutoPageBreak(true, 100);
    }

    public function Header()
    {
        $fTemplateHeader = fopen(dirname(__FILE__) . "/header.template.html", "r");
        $html = fread($fTemplateHeader, filesize(dirname(__FILE__) . "/header.template.html"));

        $mapVal = new MapVal($html);
        foreach ($this->headerData as $key => $data) {
            $mapVal->set($key, $data);
        }

        $html = mb_eregi_replace('\>\s+\<', '><', $mapVal->output());
        $html = mb_eregi_replace('\>\s+', '>', $html);
        $this->WriteHTML($html);

        $this->SetXY(188, 16);
        $this->SetFont($this->FONT_NAME, 'b', 12);
        $this->Cell(20, 5, 'แผ่นที่ ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'L', false, '', false);
    }

    public function Footer()
    {
        $fTemplateFooter = fopen(dirname(__FILE__) . '/footer.template.html', 'r');
        $html = fread($fTemplateFooter, filesize(dirname(__FILE__) . '/footer.template.html'));

        $mapVal = new MapVal($html);
        foreach ($this->footerData as $key => $data) {
            $mapVal->set($key, $data);
        }

        $html = mb_eregi_replace('\>\s+\<', '><', $mapVal->output());
        $html = mb_eregi_replace('\>\s+', '>', $html);
        $this->WriteHTML($html);
    }
}