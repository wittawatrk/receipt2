<?php

class MapVal
{
    private $master_txt = null;

    function __construct($master_txt = "")
    {
        $this->master_txt = $master_txt;
    }

    function set($key, $value)
    {
        $this->master_txt = mb_ereg_replace("{{" . $key . "}}", $value, $this->master_txt);
    }

    function output()
    {
        return $this->master_txt;
    }
}