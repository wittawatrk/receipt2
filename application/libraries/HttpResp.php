<?php

class HttpResp
{
    public static function code($code = 200)
    {
        $strHttp = "HTTP/1.1 ";
        switch ($code) {
            case 400:
                $code .= " Bad Request";
                break;
            case 401:
                $code .= " Unauthorized";
                break;
            case 402:
                $code .= " Payment Required";
                break;
            case 403:
                $code .= " Forbidden";
                break;
            case 404:
                $code .= " Not Found";
                break;
            case 405:
                $code .= " Method Not Allowed";
                break;

            case 500:
                $code .= " Internal Server Error";
                break;

            // $code = 200
            default:
                $code .= " OK";
                break;
        }
        header($strHttp . $code);
        print $code;
        return true;
    }
}