<!DOCTYPE html>
<html ng-app="receiptApp" ng-cloak="" ng-controller="rootController">
<head>
    <title>Receipt</title>
    <meta content="IE=edge" charset="tis-620" lang="TH" http-equiv="X-UA-Compatible">
    <base href="/receipt2/">
    <link rel="stylesheet" href="bower_components/angular-material/angular-material.css">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="public/foundation-icons/foundation-icons.css">
    <link rel="stylesheet" href="public/alertify/css/themes/default.min.css">
    <link rel="stylesheet" href="public/alertify/css/alertify.min.css">
    <link rel="stylesheet" href="public/css/style.css">
</head>
<body>
<?php print $uiLogout; ?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand">Receipt Creator</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li ng-class="{active: menuBar === 'receiptList'}">
                    <a ng-href="dist/receipt_list/{{receipt_type || 'product'}}">Receipt List <span class="sr-only">(current)</span></a>
                </li>
                <li class="dropdown" ng-class="{active: menuBar.indexOf('addReceipt') == 0}">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        Add Receipt
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li ng-class="{active: menuBar === 'addReceipt/product'}"><a href="dist/add_receipt/product">Product</a></li>
                        <li ng-class="{active: menuBar === 'addReceipt/service'}"><a href="dist/add_receipt/service">Service</a></li>
                        <li ng-class="{active: menuBar === 'addReceipt/service_novat'}"><a href="dist/add_receipt/service_novat">Service No Vat</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar"></nav>
<?php if (false) { ?>
    <nav role="navigation" style="background-color:#FFFFFF;border-bottom:2px solid;padding:5px;margin-bottom:10px;">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="" style="padding: 3px 3px;">
                    <img src="public/img/brand_logo.png" width="220" height="40" alt="">
                </a>
            </div>
            <div style="float:left;" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="dist/receipt_list">Receipt List</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Add Receipt
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="dist/add_receipt/product">Product</a></li>
                            <li><a href="dist/add_receipt/service">Service</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<?php } ?>
<ng-view></ng-view>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="bower_components/angular/angular.min.js"></script>
<script src="bower_components/angular-route/angular-route.min.js"></script>
<script src="bower_components/angular-resource/angular-resource.min.js"></script>
<script src="bower_components/angular-aria/angular-aria.js"></script>
<script src="bower_components/angular-animate/angular-animate.js"></script>
<script src="bower_components/angular-material/angular-material.js"></script>
<script src="bower_components/angular-messages/angular-messages.min.js"></script>
<script src="bower_components/angular-datatables/dist/angular-datatables.min.js"></script>

<script src="public/js/jquery.autocomplete.min.js"></script>
<script src="public/angular/app.js"></script>
<script src="public/angular/rootController.js"></script>
<script src="public/angular/listReceiptController.js"></script>
<script src="public/angular/addReceiptController.js"></script>
<script src="public/angular/setCustomerController.js"></script>
<script src="public/angular/editReceiptController.js"></script>
<script src="public/alertify/alertify.min.js"></script>
<script>
    setBodyMinHeight();
    window.addEventListener("resize", function () {
        setBodyMinHeight();
    });
    function setBodyMinHeight() {
        document.body.style.minHeight = (window.innerHeight + 100) + "px";
    }
</script>
</body>
</html>